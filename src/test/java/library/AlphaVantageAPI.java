package library;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojos.AlphaVantagePOJO;

public class AlphaVantageAPI {

    private static String uri = "https://www.alphavantage.co";

    public static AlphaVantagePOJO gettingStock(String stock) throws JsonProcessingException {

        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        request.headers("Content-Type", "application/json");
        Response response = request.get(uri + "/query?function=TIME_SERIES_DAILY&symbol="+stock+"&apikey=2RV43UFYL3TW8JII");

        String json = response.getBody().asString();
        ObjectMapper mapper = new ObjectMapper();

        AlphaVantagePOJO alphaVantageStock = mapper.readValue(json, AlphaVantagePOJO.class);
        return alphaVantageStock;
    }
}
