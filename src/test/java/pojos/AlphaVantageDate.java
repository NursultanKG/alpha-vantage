package pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class AlphaVantageDate {
    @JsonProperty("Meta Data")
    public MetaData metaData;

    @JsonProperty("Time Series (Daily)")
    public Time time;

    @Override
    public String toString() {
        return "{" + metaData +
                ", " + time +
                '}';
    }

    public static class MetaData {
        @JsonProperty("1. Information")
        public String Information;
        @JsonProperty("2. Symbol")
        public String Symbol;
        @JsonProperty("3. Last Refreshed")
        public String LastRefreshed;
        @JsonProperty("4. Output Size")
        public String OutputSize;
        @JsonProperty("5. Time Zone")
        public String TimeZone;

        @Override
        public String toString() {
            return "MetaData{" +
                    "Information='" + Information + '\'' +
                    ", Symbol='" + Symbol + '\'' +
                    ", LastRefreshed='" + LastRefreshed + '\'' +
                    ", OutputSize='" + OutputSize + '\'' +
                    ", TimeZone='" + TimeZone + '\'' +
                    '}';
        }
    }


    @JsonDeserialize(using = CustomDeserializer.class)
    public static class Time {
        public Map<String, Daily> dates;
        @Override
        public String toString() {
            return "Time{" + dates +
                    '}';
        }
    }

    public static class Daily {
        @JsonProperty("1. open")
        public double open;
        @JsonProperty("2. high")
        public double high;
        @JsonProperty("3. low")
        public double low;
        @JsonProperty("4. close")
        public double close;
        @JsonProperty("5. volume")
        public int volume;
        @Override
        public String toString() {
            return "{" +
                    "open=" + open +
                    ", high=" + high +
                    ", low=" + low +
                    ", close=" + close +
                    ", volume=" + volume +
                    '}';
        }
    }

    public static class CustomDeserializer extends JsonDeserializer<Time> {
        @Override
        public Time deserialize(JsonParser jp, DeserializationContext ctxt)
                throws IOException {
            Time time = new Time();
            time.dates = new LinkedHashMap<>();
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Daily> map = mapper.readValue(jp, new TypeReference<Map<String, Daily>>() {
            });
            for (String x : map.keySet()) {
                Daily daily = new Daily();
                daily.open = map.get(x).open;
                daily.high = map.get(x).high;
                daily.low = map.get(x).low;
                daily.close = map.get(x).close;
                daily.volume = map.get(x).volume;
                time.dates.put(x, daily);
            }
            return time;
        }
    }
}