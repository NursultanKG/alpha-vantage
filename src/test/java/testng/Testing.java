package testng;

import com.fasterxml.jackson.core.JsonProcessingException;
import library.AlphaVantageAPI;
import library.AlphaVantageAPI2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pojos.AlphaVantageDate;
import pojos.AlphaVantagePOJO;

public class Testing {
    private static Logger logger = LoggerFactory.getLogger(Testing.class);

    public AlphaVantagePOJO alphaVantagePOJO;

    @Test
    public void stock() throws JsonProcessingException {
        logger.info("POJO - Serialization");
        alphaVantagePOJO = AlphaVantageAPI.gettingStock("IBM");
        System.out.println(alphaVantagePOJO);
        System.out.println(alphaVantagePOJO.metaData);
        System.out.println(alphaVantagePOJO.time);

        System.out.println("====== Meta Data ======");
        System.out.println(alphaVantagePOJO.metaData.Information);
        System.out.println(alphaVantagePOJO.metaData.Symbol);
        System.out.println(alphaVantagePOJO.metaData.LastRefreshed);
        System.out.println(alphaVantagePOJO.metaData.OutputSize);
        System.out.println(alphaVantagePOJO.metaData.TimeZone);
        System.out.println("====== Daily ======");
        for (String x : alphaVantagePOJO.time.dates.keySet())
            System.out.println("Key: " + x + " Value: " + alphaVantagePOJO.time.dates.get(x));
    }

    AlphaVantageDate alphaVantageDate;

    @Test
    public void stock2() throws JsonProcessingException {
        alphaVantageDate = AlphaVantageAPI2.gettingStock("IBM");
        System.out.println(alphaVantageDate);
        System.out.println(alphaVantageDate.metaData);
        System.out.println(alphaVantageDate.time);

        System.out.println("====== Meta Data ======");
        System.out.println(alphaVantageDate.metaData.Information);
        System.out.println(alphaVantageDate.metaData.Symbol);
        System.out.println(alphaVantageDate.metaData.LastRefreshed);
        System.out.println(alphaVantageDate.metaData.OutputSize);
        System.out.println(alphaVantageDate.metaData.TimeZone);

        System.out.println("====== Daily ======");
        for (String x : alphaVantageDate.time.dates.keySet())
            System.out.println("Key: " + x + " Value: " + alphaVantageDate.time.dates.get(x));

        double expectedOpen = 123.0100;
        Assert.assertEquals(alphaVantageDate.time.dates.get("2020-08-21").open, expectedOpen);
    }
}