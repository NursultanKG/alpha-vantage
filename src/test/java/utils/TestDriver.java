package utils;

import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestDriver {

    public static Properties prop;

    @BeforeClass
    public void setup() throws IOException {
//        BasicConfigurator.configure();
        prop = new Properties();
        InputStream input = new FileInputStream("src/test/resources/config.properties");
        prop.load(input);
    }

}
