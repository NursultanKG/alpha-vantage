package utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBQuery {
    public static void executeStatement(String statement, String dbName) {
        Statement stmt = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://34.66.175.181/" + dbName;
            conn = DriverManager.getConnection(url, "tdastudents", "T3L0sd1a2t3a4)_");
            stmt = conn.createStatement();
            stmt.executeUpdate(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Statement Executed!");
    }

    public static List<List<String>> executeQueryList(String sql, String dbName) {
        // initialize array
        List<List<String>> listResults = new ArrayList<List<String>>();
        List<String> results = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://34.66.175.181/" + dbName;
            Connection conn = DriverManager.getConnection(url, "tdastudents", "T3L0sd1a2t3a4)_");
            Statement st = conn.createStatement();
            ResultSet srs = st.executeQuery(sql);
            ResultSetMetaData rsmd = srs.getMetaData();
            int columns = rsmd.getColumnCount();
            while (srs.next()) {
                results = new ArrayList<String>();
                for (int j = 0; j < columns; j++) {
                    results.add(srs.getString(j + 1));
                }
                listResults.add(results);
            }
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listResults;
    }

    public static ResultSet executeQueryResultSet(String sql, String dbName) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://34.66.175.181/" + dbName;
            Connection conn = DriverManager.getConnection(url, "tdastudents", "T3L0sd1a2t3a4)_");
            Statement st = conn.createStatement();
            ResultSet srs = st.executeQuery(sql);
            conn.close();
            return srs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
